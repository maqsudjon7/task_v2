<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home.index');
// });

Route::get('/', 'ContactBookController@index');
Route::post('/readmore/{skip}', 'ContactBookController@readMore');
Route::post('/sortBy/{column}/{type}/{take}', 'ContactBookController@sortBy');
Route::post('/refresh', 'ContactBookController@refresh');

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactBook;

class ContactBookController extends Controller
{
    protected $take = 2;  // Default amount of retrieved data from our resource

    public function index()
    {
        $persons = ContactBook::retrieveData(0, $this->take); // Retrieving innitial data 
        $the_end_of_data = ContactBook::check_the_end(0, $this->take); // Cheking whether we have more data or not
        return view('home.index', ['persons' => $persons, 'the_end_of_data' => $the_end_of_data]);
    }

    /*
        This function takes 1 parameter as argument and returns more data (by default 2)
    */

    public function readMore($skip)
    {
        $persons = ContactBook::retrieveData($skip, $this->take);
        $the_end_of_data = ContactBook::check_the_end($skip, $this->take);

        return response()->json([
            'persons' => $persons,
            'the_end_of_data' => $the_end_of_data
            
        ]);
    }

    /*
        This function takes 3 parameters as argument and returns sorted data by column
        chosed by user
    */

    public function sortBy($column, $type, $take)
    {
        $persons = new ContactBook();
        $persons->column = $column;
        
        $sortedData = $persons->sortBy(0, $take, $type);

        return response()->json([
            'persons' => $sortedData,
        ]);
    }

    /*
        This function  returns data to initial 
    */

    public function refresh()
    {
        $persons = ContactBook::retrieveData(0, $this->take);
        $the_end_of_data = ContactBook::check_the_end(0, $this->take);
        return response()->json([
            'persons' => $persons,
            'the_end_of_data' => $the_end_of_data
        ]);
    }
}

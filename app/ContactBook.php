<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \stdClass;

class ContactBook extends Model
{
    public  $column;
    protected $type = 'asc';

    /*
        This function returns acquired data from our json file 
    */

    public static function getJsonFile()
    {
        $json = file_get_contents(base_path('resources/contacts/data.json')); // Reading the file into variable
        $data = self::activeData(json_decode($json, false));

        return $data;
    }

    /*
        This function takes array as argument and returns data which is_Active property is true as an object
    */

    public static function activeData($array)
    {
        $result = new stdClass();
        $i = 0;
        foreach($array as $key => $value)
        {
            foreach($value->item as $item)
                {
                    if($item->isActive)
                        $result->$i = $item;
                }
                
            $i++;
        }

        return $result;
    }

    /*
        This function returns whether we have more data or not
    */

    public static function check_the_end($skip = 0, $take)
    {
        $amount_of_person = count((array)self::getJsonFile());
        return  $amount_of_person > $skip + $take ? false : true;
    }

    
    public static function retrieveData($skip, $take)
    {
        $data = self::getJsonFile();
        $result = [];
        $j = 0;
        $i = 0;

        foreach($data as $key => $value)
        {   
            if($j >= $skip)
                if($i < $take){
                    $result[$key] = $value;
                    $i++;
                }
            $j++;
        }
        

        return $result;
    }

    public  function sortRule($a, $b)
    {
        $a = (array) $a;
        $b = (array) $b;

        if($this->type == 'asc')
            return strcmp($a[$this->column], $b[$this->column]);
        else if($this->type == 'desc')
            return strcmp($b[$this->column], $a[$this->column]);
    }

    public  function sortBy($skip, $take, $type)
    {
        $data = $this->retrieveData($skip, $take);
        $this->type = $type;
        usort($data, array($this, 'sortRule'));
        return  $data;
    }

}

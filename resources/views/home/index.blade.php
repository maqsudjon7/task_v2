<!DOCTYPE html>
<html>


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Advance Table - FLATY Admin</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--base css styles-->
    <link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/font-awesome/css/font-awesome.min.css">

    <!--page specific css styles-->

    <!--flaty css styles-->
    <link rel="stylesheet" href="/css/flaty.css">
    <link rel="stylesheet" href="/css/flaty-responsive.css">

    <link rel="shortcut icon" href="img/favicon.html">
</head>

<body>

        <!-- BEGIN Content -->
        <div id="main-content">
            <!-- BEGIN Page Title -->
            <!-- END Page Title -->

            <!-- BEGIN Main Content -->
            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-content">
                            <div class="btn-toolbar pull-right">
                                <div class="btn-group">
                                    <a class="btn btn-circle show-tooltip eye @if($the_end_of_data) hide @endif" title="Read more" href="#"><i class="fa fa-eye"></i></a>
                                </div>
                                <div class="btn-group">
                                    <a class="btn btn-circle show-tooltip refresh" title="Refresh" href="#"><i class="fa fa-repeat"></i></a>
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <div class="table-responsive">
                                <table class="table table-advance">
                                    <thead>
                                        <tr>
                                            <th><a class="sort" data-type='asc' data-sort = "name" href="#">Name</a></th>
                                            <th><a class="sort" data-type='asc' data-sort = "gender"  href="#">Gender</a></th>
                                            <th><a class="sort" data-type='asc' data-sort = "age"  href="#">Age</a></th>
                                            <th><a class="sort" data-type='asc' data-sort = "email"  href="#">E-mail</a></th>
                                            <th><a class="sort" data-type='asc' data-sort = "phone"  href="#">Phone</a></th>
                                            <th><a class="sort" data-type='asc' data-sort = "address"  href="#">Address</a></th>
                                            <th><a class="sort" data-type='asc' data-sort = "company"  href="#">Company</a></th>
                                            <th><a class="sort" data-type='asc' data-sort = "eyeColor"  href="#">Eye color</a></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if($persons)
                                            @foreach($persons as $key => $person)
                                                <tr class="table-flag-blue">
                                                    <td>{{$person->name}}</td>
                                                    <td>{{$person->gender}}</td>
                                                    <td>{{$person->age}}</td>
                                                    <td>{{$person->email}}</td>
                                                    <td>{{$person->phone}}</td>
                                                    <td>{{$person->address}}</td>
                                                    <td>{{$person->company}}</td>
                                                    <td>{{$person->eyeColor}}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <!-- END Main Content -->

            <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a>
        </div>
        <!-- END Content -->
    </div>
    <!-- END Container -->

    <!--basic scripts-->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>



    <!--flaty scripts-->
    <script src="/js/page.js"></script>

</body>

</html>
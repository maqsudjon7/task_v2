
$(document).ready(function() {

    var skip; 

    
    $.ajaxSetup({   
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.eye').on('click', function(){
        skip = $('tbody tr').length;
        readMore();
    })

    $('.refresh').on('click', function(){
        skip = $('tbody tr').length;
        refresh();
    });



    $('a.sort').on('click', function(){

        var column = $(this).data('sort');                //Getting the column chosen by user
        var type = $(this).data('type');                  //Getting the type of sort(asc or desc)
        if(type == 'desc'){                                 
            $(this).data('type', 'asc');                    // Change to asc(if it was desc) if the user click the second time it will sort with asc option
            $(this).addClass('sort-desc');
            $('a.sort').removeClass("sort-asc");             // Removing class if there is any
        }

        else if(type == 'asc'){
            $(this).data('type', 'desc');
            $(this).addClass('sort-asc');
            $('a.sort').removeClass("sort-desc");             // Removing class if there is any
        } else{
            $(this).data('type', 'asc');             //default sorting type
            $(this).addClass('sort-asc');
            $('a.sort').removeClass("sort-asc");             // Removing class if there is any
        }

        sortBy(column, type);
    });

    
    /*
        This function retrieves 2 more data and puts it in our table
    */
    function readMore(){

        $.ajax({
            url: `/readmore/${skip}`,
            method: 'POST',
            success: function(result){
                $.each(result.persons, function(k,data){
                    
                    var item = $(
                        `<tr class="table-flag-blue">
                            <td>  ${data.name}</td>
                            <td>  ${data.gender} </td>
                            <td>  ${data.age} </td>
                            <td>  ${data.email} </td>
                            <td>  ${data.phone} </td>
                            <td>  ${data.address} </td>
                            <td>  ${data.company} </td>
                            <td>  ${data.eyeColor} </td>
                        </tr>`).hide()
                        .fadeIn(2000);
                        $('.table').find('tbody').append(item);
                    }) 
                if(result.the_end_of_data)
                    $(".eye").fadeOut(2000);

                    }
        })
    }



    function sortBy(column, type){

        var take = $('tbody tr').length;    // Getting the amount of data on table according which we will sort

        $.ajax({
            url: `/sortBy/${column}/${type}/${take}`,
            method: 'POST',
            success: function(result){
                $('.table tbody').empty();
                $.each(result.persons, function(k,data){
                    
                    var item = $(
                        `<tr class="table-flag-blue">
                            <td>  ${data.name}</td>
                            <td>  ${data.gender} </td>
                            <td>  ${data.age} </td>
                            <td>  ${data.email} </td>
                            <td>  ${data.phone} </td>
                            <td>  ${data.address} </td>
                            <td>  ${data.company} </td>
                            <td>  ${data.eyeColor} </td>
                        </tr>`);

                        $('.table').find('tbody').append(item);
                    }) 

                    }
        })
    }

    
    function refresh(){

        $.ajax({
            url: `/refresh`,
            method: 'POST',
            success: function(result){
                $('.table tbody').empty();
                console.log(result)
                $.each(result.persons, function(k,data){
                    
                    var item = $(
                        `<tr class="table-flag-blue">
                            <td>  ${data.name}</td>
                            <td>  ${data.gender} </td>
                            <td>  ${data.age} </td>
                            <td>  ${data.email} </td>
                            <td>  ${data.phone} </td>
                            <td>  ${data.address} </td>
                            <td>  ${data.company} </td>
                            <td>  ${data.eyeColor} </td>
                        </tr>`);

                        $('.table').find('tbody').append(item);
                    }) 

                if(result.the_end_of_data)
                    $(".eye").fadeOut(2000);

                else
                    $(".eye").fadeIn(2000);
                    }

                    
        })
    }


})